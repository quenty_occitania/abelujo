#!/bin/env python
# -*- coding: utf-8 -*-
# Copyright 2014 - 2022 The Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.

"""
Before Feb, 2022, when we reserved a book that we didn't have in
stock, its quantity was set to -1. It was kind of a very neat virtual
stock idea, but we stopped with this unfit good idea. Run this script
to correct this.

All cards that are linked to a reservation and have their quantity in
stock at -1 will get a +1 per reservation, so their quantity will be
back to 0 (unless it is also in boxes/returns at the same time).

Usage:

./manage.py fix_negative_reservations
"""


from __future__ import print_function
from __future__ import unicode_literals

from django.core.management.base import BaseCommand

from search.models import Reservation

# py2/3
try:
    input = raw_input
except NameError:
    pass


class Command(BaseCommand):

    help = "Find all reservations with a card whose quantity is < 0, and put it back to 0."

    def add_arguments(self, parser):
        pass

    def run(self, *args, **options):
        """
        """
        all_resas = Reservation.objects.filter(archived=False).all()
        resas = all_resas.filter(card__quantity__lt=0)
        self.stdout.write("Reservations to fix: {} on a total of {} ongoing reservations.".format(resas.count(), all_resas.count()))
        yes = input("Continue ? [Y/n]")
        if yes not in ["", "Y"]:
            print("quit.")
            exit(1)

        # for resa in resas[:5]:
        for resa in resas:
            # We iterate through reservations, not cards, so no need to adjust the quantity
            # to add to match 0…
            print("fixing (?) ", resa, resa.card.isbn, resa.card.quantity)

            resa.card.add_card(movement=False, nb=1)  # if many resas for the same card, will add up and will tend to 0.
            if resa.is_ready:
                # We didn't have it in stock, so it isn't ready.
                # We want to be notified in the reception.
                resa.is_ready = False
                resa.notified = False  # just in case.
                resa.save()

        self.stdout.write("All done.")

        resas = Reservation.objects.filter(archived=False).filter(card__quantity__lt=0)
        self.stdout.write("Remaining: {} reservations with a card whose quantity is < 0".format(resas.count()))

    def handle(self, *args, **options):
        try:
            self.run(*args, **options)
        except KeyboardInterrupt:
            self.stdout.write("User abort.")
