# -*- coding: utf-8 -*-
# Copyright 2014 - 2022 The Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.

"""
Commander via Dilicom.

Format INTERNET.

cf COMMANDE - NOTICE LIBRAIRES.pdf

> Le nom du fichier ne doit pas comporter d'espaces ni de caractères accentués, éviter également les symboles.

Si il y a une erreur dans le fichier, il est stoppé si cela est structurel et un message d'erreur automatique du serveur est transmis sur l'adresse mail que vous nous indiquerez (la vôtre ou celle du libraire)

La commande est prise en compte lorsqu'elle disparait du répertoire, ensuite le client peut la tracer sur le site de Dilicom avec ses accès utilisateurs liés à son GLN.

Le fichier de commande est conforme et a été traité sans erreur sur notre serveur de test.
Vous trouverez ci-joint le PV de mise en production qui liste les exigences et point fonctionnels que votre solution doit avoir mis en œuvre pour que nous ouvrions l'envoi des commandes réels en production.
Nous sommes a votre disposition pour en discuter et organiser les tests en conséquence.
"""

from __future__ import unicode_literals

import datetime
import os
import tempfile
from ftplib import FTP

import pendulum

LINE_A = "A0001{gencode_destinataire}"
LINE_B = "B0002{gencode_commande_par}"
LINE_C = "C0003{numero_commande}"  # must be unique for 1 year
LINE_D = "D0004{date}"
LINE_E = "E0005{mode_expedition: lieu habituel, PRISME… }{code_notation}"  # et code remise-déla, date livraison au plus tôt/au plus tard
# Ligne G optionnelle ? cf exemple.
LINE_G = "G{line_nb}{type_commande}"  # line_nb: 0006 si line_e, sinon 0005.
LINE_L = "L{line_nb}{ean13}{quantite_6_chars}"  # quantité sur 6 charactères, complétés par des 0
LINE_Q = "Q{line_nb}"

#: We command to Dilicom, which does the work to dispatch by distributor.
# Otherwise, we should send one command file per distributor.
DILICOM_GENCODE = "3035593037000"
DILICOM_GENCODE_TEST = "3015593010100"


class ValidationError(BaseException):
    pass


def valid_line_l(line):
    if not line:
        return False
    return len(line) == 24 and line.startswith('L')

def valid_ean13(ean):
    return len(ean) == 13

def valid_txt(txt):
    messages = []
    lines = txt.split('\n')
    length = len(lines)
    q_line = lines[-1]

    if not (txt.startswith('A0001') or txt.startswith(u'A0001')):
        messages.append("doesn't start with A00001")
    if not len(q_line) == 5:
        messages.append("the Q line is not of length 6")
    if not q_line.startswith('Q') or int(q_line[1:]) != length:
        messages.append("The Q line doesn't refer to the file length")
    if messages:
        raise ValidationError("\n\n".join(messages))
    return True

def format_line_nb(i):
    "Format on 4 characters"
    return "{:04}".format(i)

def get_date():
    """
    AAAAMMJJ
    """
    now = pendulum.now()
    res = now.format('%Y%m%d')
    assert len(res) == 8
    return res

def format_quantity(nb):
    """
    Format on 6 characters.
    """
    return "{:06}".format(nb)

def get_user_gln():
    res = os.getenv('DILICOM_USER')
    if not res:
        raise "No bookshop GLN found. Set it in DILICOM_USER."
    if len(res) != 13:
        print('WARN: the GLN {} seems wrong.'.format(res))
    return res

def get_ftp_username():
    return os.getenv('DILICOM_FTP_USER') or os.getenv('DILICOM_FTP_USERNAME')

def get_ftp_password():
    return os.getenv('DILICOM_PASSWORD')

def get_ftp_host():
    """
    Test host is ftp.dilicom.net
    """
    # return os.getenv('DILICOM_HOST')
    return "ftp.dilicom.net"

def get_gencode_destinataire(test=False):
    """
    Get the bookshop GLN or the Dilicom test (DILICOM_TEST is set).
    """
    if test or os.getenv('DILICOM_TEST'):
        print("INFO: using Dilicom's test GLN")
        return DILICOM_GENCODE_TEST

    return DILICOM_GENCODE

def generate_internet_format(basketcopies):
    # TODO: from Command
    """
    Generate the text to send as a file to Dilicom's FTP.

    - basketcopies: list of BasketCopies objects, containing quantity: the quantity to command, and the Card object with card.isbn.

    A0001<gencode destinataire/distributeur>
    B0002<gencode du "commandé par">
    C0003<numéro de la commande>
    D0004<date>
    E0005<mode d'expédition: lieu habituel, PRISME…> <code notation: 0 règle habituelle>
    G0005 (car pas de présence de ligne G) (type de commande, rappel référence)
    L0006<ean><quantité>
    L0007 etc
    Qxxxx = nbr de lignes de A à Q.

    # XXX: ligne R référence obligatoire ?
    """
    if not basketcopies:
        return None

    lines = []
    exceptions = []
    gencode_destinataire = get_gencode_destinataire()
    a_line = LINE_A.format(gencode_destinataire=gencode_destinataire)
    b_line = LINE_B.format(gencode_commande_par=get_user_gln())

    numero_commande = "000100"  # TODO: Command.pk
    c_line = LINE_C.format(numero_commande=numero_commande)

    d_line = LINE_D.format(date=get_date())

    e_line = "E0005090"  # mode d'expédition = livraison habituelle, code notation = habituel.
    # Spécifier Prisme ?
    # Les distributeurs ne tiennent pas compte en général des codes utilisés dans les
    # commandes EDI. Le mode d’expédition et la notation dépendent plutôt de ce qui a
    # été négocié au moment de l’ouverture du compte.

    # g_line = None

    lines += [a_line, b_line, c_line, d_line, e_line]

    offset = 1 + len(lines)  # control the line nb of subsequent L lines.
    for i, bc in enumerate(basketcopies):
        i += offset
        ean13 = bc.card.isbn
        if not valid_ean13(ean13):
            exceptions.append("Invalid EAN13: {}".format(ean13))
        line = LINE_L.format(line_nb=format_line_nb(i),
                             ean13=ean13,
                             quantite_6_chars=format_quantity(bc.quantity),
                             )
        if not valid_line_l(line):
            exceptions.append("Invalid line L: {}".format(line))
        lines.append(line)

    q_line = LINE_Q.format(line_nb=format_line_nb(len(lines) + 1))
    lines.append(q_line)

    if exceptions:
        print(exceptions)
        exit(1)

    txt = "\n".join(lines)
    print(txt)
    assert valid_txt(txt), "The final TXT is not valid. Find out why ! \n {}".format(txt)
    return txt


def FTP_send(txt, host, user, passwd,
             cmd_id=100,  # TODO: command id
             ):
    """
    Send this TXT content (string) to our FTP client.
    TODO: Once ready: send to the I/ directory.
    The remote file name is named command<id>date<date stamp>.txt
    """
    if not txt:
        print("Our txt content is void, you fool!")
        exit(1)
    if not (host and user and passwd):
        print("FTP host, user and passwd not complete: {} - {} - {}".format(host, user, passwd))
        exit(1)

    # Write content to tmp file:
    tmpfile = tempfile.NamedTemporaryFile(mode="w+", prefix="command", suffix=".txt")
    filename = tmpfile.name
    try:
        txt = txt.encode('ascii')
    except Exception as e:
        print("warn: we tried to convert our txt content to ascii, and failed: {}".format(e))
    tmpfile.write(txt)
    tmpfile.seek(0)  # to read again

    now = datetime.datetime.now()
    now_fmt = now.strftime("%Y%m%d%H%M")
    # remotefile = "I/command{}date{}.txt".format(cmd_id, now_fmt)
    remotefile = "command{}date{}.txt".format(cmd_id, now_fmt)
    print(remotefile)

    succes = True
    try:
        ftp = FTP(host, user, passwd)  # does connect() and login()
        cmd = 'STOR {}'.format(remotefile)
        print("Sending {} to {} on FTP {} user {}...".format(filename, remotefile, host, user))
        ftp.storbinary(cmd, tmpfile)
    except Exception as e:
        print("Error sending to FTP: {}".format(e))
        succes = False
    finally:
        tmpfile.close()
        ftp.quit()

    if succes:
        print("All done.")
    else:
        print("Quitting")


if __name__ == "__main__":
    # use script:
    # ./manage.py runscript test_dilicom_command
    pass
