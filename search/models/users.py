# -*- coding: utf-8 -*-
#!/usr/bin/env python
# Copyright 2014 - 2022 The Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import unicode_literals

import collections

import pendulum
from django.db import models
from django.db.models import Q
from django.utils.translation import ugettext_lazy as __  # in Meta and model fields.

from common import CHAR_LENGTH
from common import TEXT_LENGTH
from common import TimeStampedModel
# note: can't import models, circular dependencies.
from search.models.utils import Messages
from search.models.utils import get_logger
from search.models.utils import to_ascii

log = get_logger()


def generate_email_body(resas):
    """
    dev note: to use a template for emails, see mailer.py and templates/mailer/.
    """
    linebreak = "%0A"
    newline = "{} {}".format(linebreak, linebreak)
    ending = "Nous vous l'avons mise de côté.{} merci et à tout bientôt.".format(newline)
    if len(resas) == 1:
        try:
            res = "Bonjour, {} Le livre commandé: {} est bien arrivé. \
            Nous vous l'avons mis de côté. %0D%0A %0D%0A \
            Merci et à tout bientôt.".format(newline,
                                             "{} - {} {}".format(newline, resas[0].card.title, newline))  # TODO: card.title can be null.
        except Exception as e:
            log.warning("generate_email_body: trying to pre-generate the email body for reservations: {}".format(e))
            res = "Bonjour, {} Votre commande des titres suivants est bien arrivée à la librairie: {}".format(newline, newline)

    else:
        res = "Bonjour, {} Votre commande des titres suivants est bien arrivée à la librairie: {}".format(newline, newline)
        list_titles = ""
        for resa in resas:
            list_titles += " - {} {}".format(resa.card.title, linebreak)
        res += list_titles + linebreak + ending
    return res

def generate_sms_body(resas, bookshop_name="Votre librairie."):
    """
    Simple text message.
    """
    if len(resas) == 1:
        try:
            res = "Bonjour, votre commande de {} est bien arrivée. Nous vous la mettons de cote. {}"
            res = res.format(resas[0].card.title, bookshop_name)
        except Exception as e:
            log.warning("generate_sms_body: trying to pre-generate the SMS body for reservations: {}".format(e))
            res = "Bonjour, votre commande est bien arrivée à la librairie."

    else:
        res = "Bonjour, vos livres sont bien arrivés à la librairie. Nous vous les mettons de cote."
        if bookshop_name:
            res += bookshop_name

    return res


class Reservation(TimeStampedModel):
    """
    A reservation links a client to a card he reserved.

    It can have been paid, online or at the bookshop (unlikely though).

    The client will either come to the bookshop get it (and pay), or
    we have to send it by post.
    """
    class Meta:
        pass

    client = models.ForeignKey("search.Client", null=True, on_delete=models.SET_NULL)
    #: Reserve one card.
    # XXX: Shoudn't it be many?
    card = models.ForeignKey("search.Card", null=True, blank=True,
                             on_delete=models.SET_NULL,)
    #: optional: a quantity to reserve. Defaults to 1.
    nb = models.IntegerField(default=1, null=True, blank=True)
    #: This reservation is ready, we must show it to the bookshop owner.
    # With a Stripe payment, it is first not ready and then it is confirmed in the webhook.
    is_ready = models.BooleanField(default=True)
    #: This reservation is already paid. For example, online with Stripe.
    is_paid = models.BooleanField(default=False)
    #: Payment origin: Stripe?
    payment_origin = models.CharField(max_length=CHAR_LENGTH, null=True, blank=True, verbose_name=__("Origin of the payment (Stripe?)"))
    #: Do we have to send it by post?
    send_by_post = models.BooleanField(default=False)
    #: More information to remember, in a JSON (text) field.
    payment_meta = models.TextField(max_length=TEXT_LENGTH, null=True, blank=True, verbose_name=__("More data (JSON as text)"))
    #: Payment session: the answer of Stripe when creating a checkout session.
    # Contains the payment_intent ID.
    payment_session = models.TextField(max_length=TEXT_LENGTH, null=True, blank=True, verbose_name=__("Stripe payment session (internal field)"))
    #: Payment intent ID. Is returned by the creation of the Stripe session (step 1) and is present in the webhook for validation (step 2).
    payment_intent = models.CharField(max_length=CHAR_LENGTH, null=True, blank=True, verbose_name=__("Stripe payment intent ID (internal field"))

    #: If we have taken an action on this reservation.
    #: We can put the book on the side, waiting for the client or
    #: send an email.
    #: If we "reserve" a book already in stock: we mark notified to True.
    #: When we reserve + command a book not in stock, we wait it for the reception,
    #: so notified is left to False.
    # XXX: mais notified est différent de "await_reception".
    notified = models.BooleanField(default=False)
    #: This reservation is dealed with.
    archived = models.BooleanField(default=False)
    #: If it is archived, is it upon success ?
    success = models.BooleanField(default=True)

    def __unicode__(self):
        if self.client and self.card:
            return unicode("client {}, card {}".format(self.client, self.card.pk))
        elif self.client:
            return unicode("client {}, card: ???".format(self.client))
        else:
            return "Reservation (no client, no card ?!)"

    def get_absolute_url(self):
        return "/admin/search/reservation/{}/".format(self.id)

    def to_dict(self):
        res = {
            "id": self.pk,
            "archived": self.archived,
            "created": self.created,
            "client_id": self.client.id if hasattr(self, 'client') and self.client else None,
            "client_repr": self.client.__repr__() if hasattr(self, 'client') else None,
            "client": self.client.to_dict() if hasattr(self, 'client') and self.client else None,
            "card_id": self.card.id if hasattr(self, 'card') and self.card else None,
            "card": self.card.to_list(with_authors=False,
                                      with_publishers=False,
                                      with_quantity=False),
            "nb": self.nb,
            "notified": self.notified,
            "is_paid": self.is_paid,
            "payment_origin": self.payment_origin,
            "is_ready": self.is_ready,
            "success": self.success,
            "send_by_post": self.send_by_post,
            # more JSON data:
            "payment_meta": self.payment_meta,
        }
        return res

    @staticmethod
    def get_reservations(to_dict=False):
        """
        Return all current reservations.

        Discard the reservations not ready:
        - the ones reserved but not in stock, not received, thus not marked ready,
        - the ones started with an online payment, with a payment process, but not
        confirmed (thus not marked ready).
        # XXX: is_ready is used to mark the payment validated AND the card received :S
        """
        # WARN: we remove is_ready=True, thus messing with online payments?
        # This new queryset and unit test should do…
        # Ignore the ones with a payment_origin ("stripe"), but not ready (not confirmed).
        res = Reservation.objects.filter(archived=False).\
            exclude(Q(payment_origin__isnull=False) & Q(is_ready=False)).\
            order_by('client__name')
        return res

    @staticmethod
    def client_has_reservations(pk):
        """
        Return: int
        """
        return Reservation.objects.filter(client=pk, archived=False, is_ready=True).count()

    @staticmethod
    def client_has_ready_reservations(pk):
        res = Reservation.objects.filter(client=pk, archived=False, is_ready=True,
                                          card__quantity__gte=0)
        return res.count()

    @staticmethod
    def nb_ongoing():
        res = Reservation.objects.filter(archived=False, is_ready=True).count()
        return res

    @staticmethod
    def group_by_client(reservations):
        """
        Group this list of reservations by client.

        Returns: a list of tuples: client -> list of reservations.
        """
        seen = collections.OrderedDict()
        # group by client.
        for resa in reservations:
            if seen.get(resa.client):
                seen[resa.client].append(resa)
            else:
                seen[resa.client] = [resa]

        return seen.items()

    @staticmethod
    def generate_email_bodies(tuples):
        """
        From this list of tuples from group_by_client, add an email_body field.
        """
        for client_reservations in tuples:
            client = client_reservations[0]
            resas = client_reservations[1]
            body = generate_email_body(resas)
            if client:
                client.reservation_email_body = body
        return tuples

    @staticmethod
    def generate_sms_bodies(tuples, bookshop_name="Votre librairie."):
        """
        From this list of tuples from group_by_client, add a reservation_sms_body field.
        """
        for client_reservations in tuples:
            client = client_reservations[0]
            resas = client_reservations[1]
            body = generate_sms_body(resas, bookshop_name=bookshop_name)
            if client:
                client.reservation_sms_body = body
        return tuples

    def is_quite_old(self):
        """
        Return True if this Reservation is older than 2 weeks.
        """
        OLD_RESERVATION_DAYS = 14
        try:
            now = pendulum.now()
            created = pendulum.instance(self.created)
            diff = created.diff(now)
            if diff.days >= OLD_RESERVATION_DAYS:
                return True
        except Exception as e:
            log.warning(e)
        return False

    @staticmethod
    def get_card_reservations(card, to_dict=False):
        """
        Get the ongoing reservations for this card (the ones not archived).
        Show them on the card page.

        - card: int or object
        """
        card_id = card
        if isinstance(card, models.base.Model):
            card_id = card.id
        res = Reservation.objects.filter(card=card_id, archived=False)
        if to_dict:
            res = [it.to_dict() for it in res]
        return res

    @staticmethod
    def putaside(card, client):
        """
        Confirm that a reservation was received and that we put this book
        aside for the client. Remove 1 exemplary from the stock
        (beceause it was added by the reception).

        To create a reservation, use Card.reserve.
        """
        msgs = Messages()
        resa = Reservation.objects.filter(card=card,
                                          client=client,
                                          archived=False,
                                          # A reservation when book
                                          # already in stock (minority
                                          # of cases) is supposed to
                                          # be "notified" at the time of
                                          # the command.
                                          notified=False).first()
        if not resa:
            msgs.add_error("No reservation to process for this card and client.")
            log.info("No reservation to process.")
            return False, msgs.msgs
        # C'est aussi putaside appelé lors du "OK".
        # Faire la différence entre "mettre de côté" et "réserver pour une commande".
        try:
            # si la quantité en stock est 0 (ou -1 ?), ça veut dire qu'on l'avait réservée
            # et commandée, qu'on ne l'avait PAS déjà mise de côté,
            # donc on ne veut PAS faire un -1 ici.
            #
            # Logique: on ne peut PAS mettre de côté un titre qu'on n'a pas en stock.
            # Arrêter avec le -1.
            #
            # euh… mais si livre déjà en stock, qu'on en reçoit un nouveau…
            # pourquoi décrémenter aussi ?
            #
            # => si on le reçoit et qu'on le met de côté: on décrémente.
            if not resa.is_ready:
                card.remove_card()
                resa.is_ready = True

            resa.notified = True
            resa.save()
        except Exception as e:
            msgs.add_error(u"Could not put card {} aside: {}".format(card.pk, e))
            log.error(msgs.msgs)
            return False, msgs.msgs

        return True, msgs.msgs


class ContactSMS(TimeStampedModel):
    """
    Text messages sent to contacts.

    A contact was sent many SMS.
    A SMS is sent to one contact.
    """
    class Meta:
        ordering = ("created",)

    client = models.ForeignKey("search.Client", null=True, on_delete=models.SET_NULL)
    body = models.TextField(max_length=TEXT_LENGTH, null=True, blank=True, verbose_name=__("SMS body"))
    data = models.TextField(max_length=TEXT_LENGTH, null=True, blank=True, verbose_name=__("SMS body"))


class Contact(models.Model):
    """
    A contact information (client or supplier), with payment information, etc.

    Distinguish the informations between a physical or a moral person ?
    """
    class Meta:
        ordering = ("firstname",)
        abstract = True

    name = models.CharField(max_length=CHAR_LENGTH)
    name_ascii = models.CharField(max_length=CHAR_LENGTH, null=True, blank=True, editable=False)
    firstname = models.CharField(blank=True, null=True, max_length=CHAR_LENGTH)
    firstname_ascii = models.CharField(max_length=CHAR_LENGTH, null=True, blank=True, editable=False)
    mobilephone = models.CharField(blank=True, null=True, max_length=CHAR_LENGTH)
    telephone = models.CharField(blank=True, null=True, max_length=CHAR_LENGTH)
    email = models.EmailField(blank=True, null=True)
    website = models.CharField(blank=True, null=True, max_length=CHAR_LENGTH)

    # Official numbers
    company_number = models.CharField(blank=True, null=True, max_length=CHAR_LENGTH, verbose_name=__("The company's registered number (State's industry chamber)"))
    bookshop_number = models.CharField(blank=True, null=True, max_length=CHAR_LENGTH, verbose_name=__("The bookshop's official number."))

    # Address
    address1 = models.CharField(blank=True, null=True, max_length=CHAR_LENGTH)
    address2 = models.CharField(blank=True, null=True, max_length=CHAR_LENGTH)
    zip_code = models.CharField(blank=True, null=True, max_length=CHAR_LENGTH)
    city = models.CharField(blank=True, null=True, max_length=CHAR_LENGTH)
    state = models.CharField(blank=True, null=True, max_length=CHAR_LENGTH)
    country = models.CharField(blank=True, null=True, max_length=CHAR_LENGTH)

    presentation_comment = models.TextField(blank=True, null=True, max_length=TEXT_LENGTH, verbose_name=__("A comment to add after the default presentation, which contains name, address, contact and official number. Can be useful when the bookshop is officially administrated by another entity. This appears on bills."))

    # Bank/payment details
    checks_order = models.CharField(blank=True, null=True, max_length=CHAR_LENGTH, verbose_name=__("Checks order (if different from name)"))
    checks_address = models.CharField(blank=True, null=True, max_length=CHAR_LENGTH, verbose_name=__("Checks address (if different than address)"))
    bank_IBAN = models.CharField(blank=True, null=True, max_length=CHAR_LENGTH, verbose_name=__("IBAN"))
    bank_BIC = models.CharField(blank=True, null=True, max_length=CHAR_LENGTH, verbose_name=__("BIC"))
    is_vat_exonerated = models.BooleanField(default=False, verbose_name=__("Exonerated of VAT?"))

    # SMS History.
    sms_history = models.ManyToManyField(ContactSMS,
                                         blank=True,
                                         related_name="%(app_label)s_%(class)s_related",
                                         # related_query_name="%(app_label)s_%(class)ss",
                                         verbose_name=__("History of text messages sent to this contact (SMS)"))

    comment = models.TextField(blank=True, null=True)

    def save(self, *args, **kwargs):
        """
        Add:
        - name_ascii
        - firstname_ascii
        """
        res = to_ascii(self.name)
        if res:
            self.name_ascii = res
        res = to_ascii(self.firstname)
        if res:
            self.firstname_ascii = res

        super(Contact, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return "/admin/search/{}/{}".format(self.__class__.__name__.lower(),
                                            self.id)

    def to_dict(self):
        rep = self.__repr__()
        return {'id': self.id,
                'name': self.name.upper(),
                'firstname': self.firstname.capitalize(),
                'mobilephone': self.mobilephone,
                '__repr__': rep,
                'repr': rep,  # in templates, can't use __repr__
                'url': self.get_absolute_url(),
        }

    def __repr__(self):
        return u"{} {} - {}".format(self.name, self.firstname, self.mobilephone)

    def is_correct_phone_number(self, phone):
        """
        It's possible that the user doesn't enter a valid phone number
        -but some digits that are enough for him to understand as a full phone number.
        A basic check imposes.
        It depends on the client's country.

        - phone: the phone number (string).
        """
        if not phone:
            return False
        if not self.country or (self.country and self.country.lower() == 'france'):
            if len(phone) < 6:
                return False
        return True

    def mobilephone_for_sms(self):
        """
        Try to format the mobilephone (if any) to an international format,
        suitable to the external app used to send SMS.

        Return: False or a string.
        """
        if not self.mobilephone:
            return False
        if not self.is_correct_phone_number(self.mobilephone):
            return False
        if self.mobilephone.startswith('+'):
            return self.mobilephone
        try:
            res = ""
            if not self.country or (self.country and self.country.lower() == 'france'):
                if self.mobilephone[:2] in ['06', '07']:
                    # Currently, adapting to Twilio.
                    res = '+33' + self.mobilephone[1:]
                elif self.mobilephone.startswith('0033'):
                    res = '+33' + self.mobilephone[4:]
                return res
        except Exception as e:
            log.warning("Error trying to format the mobilephone for SMS of client {}: {}".format(self.pk, e))
            return self.mobilephone

        # Other countries: just write a phone number in the right international format.
        return self.mobilephone

    def save_sms(self, body, data=None):
        """
        Save SMS.
        """
        if not body:
            log.warning("Body message is null. Did you really want to save this SMS?")
        try:
            sms = ContactSMS(client=self,
                             body=body)
            sms.save()
            if data:
                sms.data = data
                sms.save()

            return sms
        except Exception as e:
            log.error("SMS to contact {} could not be created: {}".format(self.pk, e))

    def last_sms_sent(self):
        try:
            res = ContactSMS.objects.filter(client__pk=self.pk).order_by("created").last()
            return res
        except Exception as e:
            log.error("Error getting the last sent SMS to client {}: {}".format(self.pk, e))

    def recent_sms(self):
        """
        Return: a short list of the last sent SMS.
        """
        try:
            MAX = 5
            res = ContactSMS.objects.filter(client__pk=self.pk).order_by("-created")[:MAX]
            return res
        except Exception as e:
            log.error("Error getting the last sent SMS to client {}: {}".format(self.pk, e))

    def nb_sms_sent(self):
        """
        Return: the number of SMS sent to this contact (int).
        """
        try:
            res = ContactSMS.objects.filter(client__pk=self.pk).count()
            return res
        except Exception as e:
            log.error("Error getting the last sent SMS to client {}: {}".format(self.pk, e))
            return -1


class Client(Contact):
    """
    A client of the bookshop.
    He can reserve books, buy coupons, etc.

    A client can be an individual or an institution (schools, libraries (bibliothèques)…).
    """
    #: Is this client an institution?
    is_institution = models.BooleanField(default=False, verbose_name=__("Is this client an institution?"))
    #: Default discount. Specially useful when the client is an institution (collectivité).
    discount = models.IntegerField(blank=True, null=True, verbose_name=__("Discount (%)"))

    #: A client is linked to sells.
    #
    #: When we register a new client on this software, we might want
    # to port data from our previous system.
    #: One data is the number of sells he currently has.
    # So, his fidelity card is not reset to 0.
    initial_sells_quantity = models.IntegerField(default=0, verbose_name=__("The number of registered sells the client has on the previous system."))

    # Other noticeable methods:
    # Sell.count_client_soldcards(client)

    def __repr__(self):
        return u"{} {}".format(self.name, self.firstname)

    def repr(self):
        # for templates. Method cannot start with an underscore. Stupid templates.
        return self.__repr__()

    def ascii_repr(self):
        """
        Return NAME and firstname, without accents (for card page client
        selection with browser's built-in autocomplete).
        """
        return u"{} {}".format(to_ascii(self.name).upper(), to_ascii(self.firstname))

    def __unicode__(self):
        return unicode("{} {}".format(self.name, self.firstname))

    def get_absolute_url(self):
        return "/admin/search/client/{}/".format(self.id)

    def to_dict(self):
        # res = super(Contact, self).to_dict()
        rep = self.__repr__()
        return {'id': self.id,
                'name': self.name,
                'firstname': self.firstname,
                'mobilephone': self.mobilephone if hasattr(self, 'mobilephone') else "",
                'email': self.email if hasattr(self, 'email') else "",
                '__repr__': rep,
                'repr': rep,  # in templates, can't use __repr__
                'ascii_repr': self.ascii_repr(),
                'url': self.get_absolute_url(),
                # addition:
                'is_institution': self.is_institution,
                'discount': self.discount,
        }

    @staticmethod
    def get_clients(to_dict=False):
        """
        Return a list of all clients, sorted alphabetically.
        """
        res = Client.objects.all().order_by("name")
        if to_dict:
            res = [it.to_dict() for it in res]
        return res

    @staticmethod
    def get_individuals(to_dict=False):
        """
        Return a list of all individual clients.
        """
        res = Client.objects.filter(is_institution=False).all()
        if to_dict:
            res = [it.to_dict() for it in res]
        return res

    @staticmethod
    def get_institutions(to_dict=False):
        """
        Return a list of all institutions, not individuals.
        """
        res = Client.objects.filter(is_institution=True).all()
        if to_dict:
            res = [it.to_dict() for it in res]
        return res

    @staticmethod
    def search(query, to_dict=False, limit=20):
        try:
            query_ascii = to_ascii(query)
        except Exception as e:
            log.warning("Client search: error with to_ascii: {}".format(e))
            query_ascii = query
        res = Client.objects.filter(Q(name_ascii__icontains=query_ascii) |
                                    Q(firstname_ascii__icontains=query_ascii))[:limit]
        if to_dict:
            res = [it.to_dict() for it in res]

        return res

    def reserve(self, card, nb=1, send_by_post=False, is_paid=False,
                is_ready=False,
                payment_origin=None, payment_meta=None, payment_session=None,
                payment_intent=None):
        """
        Reserve this card at this quantity.
        Create a Reservation object and decrement it from the stock,

        TODO: decrement IIF already in stock?

        If its quantity gets lower than 0, add it to the commands.

        - card: card object.

        Return a tuple: Reservation object, created? (boolean).
        """
        # XXX: lol, same as putaside ??
        if isinstance(card, int):
            log.error("reserve: we don't accept card_id anymore but a Card object.")

            return None, False

        # If already in stock, it is supposed to be ready: the client knows it.
        # (this happens in a minority of cases. Most of the times, we must command a book,
        # thus the reservation isn't "ready" yet and it will be at reception).
        if card.quantity > 0:
            is_ready = True

        card_id = card.id
        resa, created = Reservation.objects.get_or_create(
            client=self,
            card_id=card_id,
            nb=nb,
            archived=False,
            send_by_post=send_by_post,
            is_paid=is_paid,
            is_ready=is_ready,
            payment_origin=payment_origin,
            payment_meta=payment_meta,
            payment_session=payment_session,
            payment_intent=payment_intent,
        )

        # If we don't need to command: mark notified.
        if card.quantity > 0:
            resa.notified = True

        resa.save()

        # Command (if needed).
        if card.quantity <= 0:
            card.add_to_auto_command()

        # Decrement card from stock if in stock.
        # Is that the right strategy? We consider the book isn't in stock anymore
        # for other people to find it. The sell won't decrement it a second time.
        # Otherwise, that means we will wait for the book from a command.
        if card.quantity > 0:
            card.remove_card()

        return resa, created

    def cancel_reservation(self, card):
        """
        Cancel the Reservation object, thus increment the card in stock too.

        If it was in the command list, remove it.
        """
        try:
            resa = Reservation.objects.filter(client=self, card_id=card.id, archived=False).first()
            if resa:
                # re-add one exemplary in stock only if the book was put
                # aside because it was physically here.  Might as well
                # stop this -1/+1…
                if resa.is_ready and resa.notified:
                    card.add_card()
                resa.archived = True
                resa.success = False  # XXX: say it is a manual cancelation?
                resa.save()

            # Remove from the command list if needed.
            # It's possible we used the +1 button,
            # or sold the book to the client without saying so in the Sell view.
            # If the qty to command reaches 0 it will still be visible on the list,
            # but at a 0 quantity to command.
            # That's ok, that way we see it when we double check the command list.
            if card.quantity > 0:
                if card.quantity_to_command() > 0:
                    card.add_to_auto_command(nb=-1)

        except Exception as e:
            log.error(u"Error canceling reservation {}: {}".format(self.pk, e))
            return False
        return True

    def has_reservations(self):
        """
        Has this client ongoing, open reservations?

        We check when we enter this client in a sell.

        Return: int (0 or the number of reservations).
        """
        return Reservation.objects.filter(client=self.pk, archived=False).count()

    @staticmethod
    def client_has_reservations(pk):
        return Reservation.objects.filter(client=pk, archived=False, is_ready=True).count()


class Bookshop(Contact):
    """
    Me, the bookshop. My address and payment details.
    """
    pass

    def __repr__(self):
        return "Bookshop {}".format(self.name)

    @staticmethod
    def name():
        # mmh, there is also Preferences.asso_name...
        try:
            bookshop = Bookshop.objects.first()
            if bookshop:
                return bookshop.name
        except Exception as e:
            log.warning("Could not find the bookshop name: {}".format(e))


class BillCopies(models.Model):
    card = models.ForeignKey("search.Card")
    bill = models.ForeignKey("Bill")
    quantity = models.IntegerField(default=0)

    def __unicode__(self):
        return "for bill {} and card {}: x{}".format(self.bill.id, self.card.id, self.quantity)


class Bill(TimeStampedModel):
    """A bill represents the cost of a set of cards to pay to a
    someone (client, distributor...).

    We can have many bills for a single command (if some cards are
    sent later for example).

    When needed we want to associate a bill to a Card we buy.

    When we generate a bill, it's ok not to save all fields, but we need a unique ID (the pk).
    """
    # created and modified fields
    ref = models.CharField(max_length=CHAR_LENGTH, null=True, blank=True)
    name = models.CharField(max_length=CHAR_LENGTH)
    # distributor = models.ForeignKey("search.distributor", null=True)
    #: we must pay the bill at some time (even if the received card is
    #: not sold, this isn't a deposit!)
    due_date = models.DateField(null=True, blank=True)
    #: total cost of the bill, without taxes.
    total_no_taxes = models.FloatField(null=True, blank=True)
    #: shipping costs, with taxes.
    shipping = models.FloatField(null=True, blank=True)
    #: reference also the list of cards, their quantity and their discount.
    copies = models.ManyToManyField("search.Card", through="BillCopies", blank=True)
    #: total to pay.
    total = models.FloatField(null=True, blank=True)

    def __unicode__(self):
        return "{}, total: {}, due: {}".format(self.name, self.total, self.due_date)

    @property
    def long_name(self):
        """
        """
        return "{} ({})".format(self.name, self.ref)

    def add_copy(self, card, nb=1):
        """Add the given card to this bill.
        """
        created = False
        try:
            billcopies, created = self.billcopies_set.get_or_create(card=card)
            billcopies.quantity += nb
            billcopies.save()
        except Exception as e:
            log.error(e)

        return created
