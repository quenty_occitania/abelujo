# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0050_auto_20220107_1222'),
    ]

    operations = [
        migrations.AddField(
            model_name='card',
            name='lang',
            field=models.CharField(max_length=200, null=True, verbose_name='Langue', blank=True),
        ),
    ]
