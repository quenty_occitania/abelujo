# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

from search.models import Publisher

def populate_name_ascii(app, schema_editor):
    pubs = Publisher.objects.all()
    for it in pubs:
        # should do it:
        it.save()


def noop(app, schema_editor):
    # nothing special to migrate backwards.
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0056_auto_20220210_1204'),
    ]

    operations = [
        migrations.AddField(
            model_name='publisher',
            name='name_ascii',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.RunPython(populate_name_ascii, noop),
    ]
