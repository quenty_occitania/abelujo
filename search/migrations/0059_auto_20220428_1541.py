# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0058_auto_20220228_1822'),
    ]

    operations = [
        migrations.AddField(
            model_name='basket',
            name='sold_date',
            field=models.DateField(null=True, verbose_name='date sold', blank=True),
        ),
    ]
